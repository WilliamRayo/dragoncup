/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
        if($('#pictures-demo').lenght) {
          $('#pictures-demo').slippry({
              // general elements & wrapper
              slippryWrapper: '<div class="sy-box pictures-slider" />', // wrapper to wrap everything, including pager

              // options
              adaptiveHeight: false, // height of the sliders adapts to current slide
              captions: false, // Position: overlay, below, custom, false

              // pager
              pager: false,

              // controls
              controls: false,
              autoHover: false,

              // transitions
              transition: 'kenburns', // fade, horizontal, kenburns, false
              kenZoom: 140,
              speed: 5000 // time the transition takes (ms)
          });
         }

         var $window = $(window);

         if($('.dropdown').length){

             if($window.width() > 1201){
                 $('.dropdown > a').addClass('dropdown-toggle disabled').attr('data-toggle', 'dropdown');
             }else{
                 $('.dropdown > a').addClass('dropdown-toggle').attr('data-toggle', 'dropdown');
             }

         }


          $('.gallery_foto_holder').each(function() {
            $(this).magnificPopup({
              delegate: 'a', // child items selector, by clicking on it popup will open
              type: 'image',
              gallery: {
                  // options for gallery
                  enabled: true
              }
              // other options
            });
          });


         $('.video_play').magnificPopup({
             type:'iframe',
             iframe: {
                 markup: '<div class="mfp-iframe-scaler">'+
                 '<div class="mfp-close"></div>'+
                 '<iframe width="100%" height="315" src="https://www.youtube.com/embed/WIFXCSILxQU" frameborder="0" allowfullscreen></iframe>'+
                 '</div>'
             }
          });

          if($('.js-multiple-select').length){
            $('#discipline-comp-0').select2();
          }

          //Function for reinit select2
          function initializeSelect2(selectElementObj) {
            selectElementObj.select2({
              width: "100%",
            });
          }

          var competitorNum = 1;

          $('.js-clone-btn').click(function(){
            $(".js-competitor-clone")
              .clone().removeClass('js-competitor-clone d-none')
              .appendTo( ".competitors" )
              .attr({
                'data-comp': competitorNum
              });
            $("div[data-comp="+ competitorNum +"] h3").html('<strong>' + (competitorNum+1) +'. Competitor</strong>');
            $("div[data-comp="+ competitorNum +"] .competitor-name label").attr('for', 'name-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-name input").attr('id', 'name-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-name input").attr('name', 'name-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-surname label").attr('for', 'surname-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-surname input").attr('id', 'surname-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-surname input").attr('name', 'surname-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-birthdate label").attr('for', 'birthdate-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-birthdate input").attr('id', 'birthdate-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-birthdate input").attr('name', 'birthdate-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-sex label").attr('for', 'sex-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-sex select").attr('id', 'sex-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-sex select").attr('name', 'sex-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-weight label").attr('for', 'weight-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-weight input").attr('id', 'weight-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-weight input").attr('name', 'weight-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-belt label").attr('for', 'belt-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-belt select").attr('id', 'belt-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-belt select").attr('name', 'belt-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-year label").attr('for', 'year-training-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-training select").attr('id', 'year-training-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-training select").attr('name', 'year-training-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-training select").attr('name', 'year-training-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-discipline .select2").remove();
            $("div[data-comp="+ competitorNum +"] .competitor-discipline select").val(null).trigger('change');
            $("div[data-comp="+ competitorNum +"] .competitor-discipline select").attr('id', 'discipline-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-discipline select").attr('multiple', 'multiple');
            $("div[data-comp="+ competitorNum +"] .competitor-discipline select").attr('data-select2-id', 'discipline-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-discipline select").attr('name', 'discipline-comp-' + competitorNum+'[]');
            $("div[data-comp="+ competitorNum +"] .competitor-accommodation label").attr('for', 'accomodation-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-accommodation select").attr('id', 'accomodation-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .competitor-accommodation select").attr('name', 'accomodation-comp-' + competitorNum);
            $("div[data-comp="+ competitorNum +"] .input-price-comp").attr('name', 'price-comp-' + competitorNum);

            //Reinit select2 for new row
            var selectName = $('#discipline-comp-' + competitorNum);
            initializeSelect2(selectName);

            competitorNum++;
          });

          $('.js-delete-btn').click(function(){
            if(competitorNum > 1) {
              $("div[data-comp="+ (competitorNum - 1) +"]").remove();

              competitorNum--;
            }
          });


          $(document).on('change', '.js-multiple-select', function(){
            var selectVal = $(this).val();
            var cena = 0;

            $.map(selectVal, function(val, i){
              cena += parseInt($('.discipline-prices div[data-name="'+ val +'"]').attr('data-price'));
            });

            $(this).closest('.row').find('.competitor-price span strong').text(cena);
            $(this).closest('.row').find('.input-price-comp').val(cena);

            var totalPrice = 0;
            $('.competitor-price span strong').each(function(){
              var comeptitorPrice = parseInt($(this).text());

              totalPrice += comeptitorPrice;
            });

            $('.total-price span strong').text(totalPrice);
            $('.input-total-price').val(totalPrice);
          });

          $('.send-form').click(function(e){
            $('.wpcf7-response-output').hide();
            var validationInput = 0;
            var validationSelect = 0;

            $('div[data-comp] input:not(".select2-search__field"):not(".input-price-comp")').each(function(){
              $(this).removeClass('is-invalid');

              if($(this).val().length < 1) {
                $(this).addClass('is-invalid');
                validationInput++;
              }
            });

            $('div[data-comp] select').each(function(){
              $(this).removeClass('is-invalid');

              if($(this).val() === "") {
                $(this).addClass('is-invalid');
                validationSelect++;
              }
            });

            console.log(validationInput + '-input, select- ' + validationSelect);

            if( validationInput !== 0 || validationSelect !== 0 ) {
              e.preventDefault();
              $('.wpcf7-response-output').addClass('wpcf7-validation-errors').text('Některá pole obsahují chybu. Opravte prosím zadané údaje a zkuste to znovu.(One or more fields have an error. Please fix it and try it again.)').show();
            }
          });

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
