<header class="header">
	  <nav class="navbar navbar-toggleable-lg navbar-inverse">
		  <div class="container">
			  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
			  </button>
			  <a class="navbar-brand" href="/">
				  <img src="<?= get_template_directory_uri().'/dist/images/logo.png' ?>" alt="Dragon cup">
			  </a>

              <div id="navbar" class="navbar navbar-collapse collapse">
				  <div class="menu_holder">
					  <?php wp_nav_menu( array(
						  'theme_location' => 'primary_navigation',
						  'items_wrap'     => '<ul class="%2$s nav navbar-nav">%3$s</ul>',
					  ) ); ?>
				  </div>
				  <div class="face_holder">
					  <a href="//www.facebook.com/DragonCupFest/?fref=ts" target="_blank">
						  <i class="fa fa-facebook-square" aria-hidden="true"></i>
					  </a>
				  </div>
				  <div class="dropdown language_holder">
					  <button class="btn btn-secondary dropdown-toggle" type="button" id="language_selector" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  <?= strtoupper(ICL_LANGUAGE_CODE)?>
					  </button>
					  <div class="dropdown-menu" aria-labelledby="language_selector">
                          <?php
                          $jazyky = icl_get_languages('skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str');
                          foreach($jazyky as $jazyk){
                              echo '<a class="dropdown-item" href="'.$jazyk['url'].'">'.strtoupper($jazyk['code']).'</a>';
                          }
                          ?>
					  </div>
				  </div>
              </div>
		  </div>
	  </nav>



</header>
