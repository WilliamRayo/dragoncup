<?php use Roots\Sage\Titles; ?>

<div class="page-header">
	<h3 class="heading_with_line"><?= Titles\title(); ?></h3>
</div>

<?php the_content(); ?>
<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
