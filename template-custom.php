<?php
/**
 * Template Name: Homepage
 */
?>

<?php /*while (have_posts()) : the_post(); */ ?><!--
  <?php /*get_template_part('templates/page', 'header'); */ ?>
  <?php /*get_template_part('templates/content', 'page'); */ ?>
--><?php /*endwhile; */ ?>

<div class="home_slider_holder">
    <div class="home_slider_img_holder">
        <ul id="pictures-demo">
            <?php
            $index = 0;
            foreach (get_field('fotky_slideru') as $slide) { ?>
                <li>
                    <img <?= $index == 0 ? 'class="active"' : '' ?> src="<?= $slide['url'] ?>" alt="slide">
                </li>
                <?php
                $index++;
            }
            ?>
        </ul>
    </div>
    <div class="home_slider_overlay"></div>
    <div class="home_slider_text">
        <h1><?= get_field('nadpis_eventu'); ?></h1>
        <div class="slider_right_side">
            <div class="event_holder">
                <h2 class="event_date"><?= get_field('datum_eventu'); ?></h2>
                <h2 class="event_place"><?= get_field('mesto'); ?></h2>
                <h3><?= get_field('lokace'); ?></h3>
            </div>
            <div class="video_holder">
                <a class="video_play" href="https://www.youtube.com/embed/WIFXCSILxQU"><?= __('Play the video', 'dragon') ?></a>
            </div>
        </div>
    </div>
</div>
<div class="program_holder homepage_section">
    <div class="container">
        <div class="text-center">
            <h3 class="heading_with_line"> <?= __('Program', 'dragon') ?></h3>
        </div>
        <div class="program_items_holder">
            <div class="program_item military">
                <h4><?= get_field('nadpis_leveho_boxu') ?></h4>
                <?= get_field('obsah_leveho_boxu') ?>
                <a class="btn-border" href="<?= get_field('odkaz_leveho_boxu') ?>"><?= __('Show more', 'dragon') ?></a>
            </div>

            <div class="program_item program_item_black bojova_umeni">
                <h4><?= get_field('nadpis_prostredniho_boxu') ?></h4>
                <?= get_field('obsah_prostredniho_boxu') ?>
                <a class="btn-border" href="<?= get_field('odkaz_prostredniho_boxu') ?>"><?= __('Show more', 'dragon') ?></a>
            </div>

            <div class="program_item fitness">
                <h4><?= get_field('nadpis_praveho_boxu') ?></h4>
                <?= get_field('obsah_praveho_boxu') ?>
                <a class="btn-border" href="<?= get_field('odkaz_prostredniho_boxu') ?>"><?= __('Show more', 'dragon') ?></a>
            </div>
        </div>
    </div>
</div>
<div class="popis_akce_holder homepage_section">
    <div class="container">
        <div class="text-center">
            <h3 class="heading_with_line"><?= get_field('nadpis_popisu') ?></h3>
            <div class="popis_text">
                <?= get_field('popis') ?>
            </div>
        </div>
    </div>
</div>
<?php
$args = array(
    'post_type' => 'reference',
    'orderby' => 'date',
    'order' => 'DESC',
    'posts_per_page' => -1,
);


$posty = new \WP_Query($args);
$index = 0;
?>
<div class="reference_holder">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php
            for ($i = 0;$i<$posty->post_count;$i++){ ?>
                <li data-target="#carouselExampleIndicators" data-slide-to="<?= $i ?>" <?= $i==0?'class="active"':''?>></li>
           <?php }
            ?>
        </ol>
        <div class="carousel-inner" role="listbox">
            <?php
            while ($posty->have_posts()) : $posty->the_post(); ?>
                <div class="carousel-item <?= $index == 0 ? 'active' : '' ?>">
                    <img class="d-block hidden-md-down img-fluid"
                         src="<?= get_the_post_thumbnail_url(get_the_ID(), 'full') ?>" alt="First slide">
                    <div class="reference_overlay"></div>
                    <div class="carousel-caption">
                        <p class="ref_text"><?= get_the_content() ?></p>
                        <p class="ref_autor"><?= get_the_title(); ?></p>
                    </div>
                </div>
                <?php
                $index++;
            endwhile;
            wp_reset_query();
            ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<div class="home_kontakt_holder homepage_section">
    <div class="container">
        <div class="text-center">
            <h3 class="heading_with_line"><?= __('Where to find us?', 'dragon') ?></h3>
        </div>
        <div class="row">
            <div class="col-md-8">
                <?= get_field('mapa') ?>
            </div>
            <div class="col-md-4 home_kontakt_info">
                <?= get_field('popis_kontakt') ?>
            </div>
        </div>
    </div>
</div>
