<?php
/**
 * Template Name: Galerie
 */

use Roots\Sage\Titles;
while (have_posts()) : the_post();
	get_template_part('templates/page', 'header');

?>

<div class="container page_content_holder">
	<?php
	get_template_part('templates/content', 'page');
	?>
</div>

<?php endwhile; ?>
	
	<?php
	$args = array(
	    'post_type' => 'galerie',
	    'orderby' => 'date',
	    'order' => 'DESC',
	    'posts_per_page' => 3,
	);
	
	
	$posty = new \WP_Query($args);
	$index = 0;
	while ($posty->have_posts()) : $posty->the_post(); ?>
		
		
		<div class="gallery_item_holder <?= $index % 2 ? '' : 'gallery_gray_bg' ?> homepage_section">
			<div class="container">
				<div class="row">
					<div class="col-lg-5 col-md-6 col-12">
						<h3><?php the_title(); ?></h3>
						<p><?php the_content(); ?></p>
					</div>
					<div class="col-lg-7 col-md-6 col-12 gallery_foto_holder">
						<?php foreach (get_field('fotky') as $fotka){ ?>
							<a href="<?= $fotka['url'] ?>">
								<img src="<?= $fotka['sizes']['thumbnail'] ?>" alt="">
							</a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		
	<?php
	
	$index++;
	endwhile;
	wp_reset_query();
	?>
	

