<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup()
{
    // Enable features from Soil when plugin is activated
    // https://roots.io/plugins/soil/
    add_theme_support('soil-clean-up');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-relative-urls');

    // Make theme available for translation
    // Community translations can be found at https://github.com/roots/sage-translations
    load_theme_textdomain('sage', get_template_directory() . '/lang');

    // Enable plugins to manage the document title
    // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
    add_theme_support('title-tag');

    // Register wp_nav_menu() menus
    // http://codex.wordpress.org/Function_Reference/register_nav_menus
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage')
    ]);

    // Enable post thumbnails
    // http://codex.wordpress.org/Post_Thumbnails
    // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
    // http://codex.wordpress.org/Function_Reference/add_image_size
    add_theme_support('post-thumbnails');

    // Enable post formats
    // http://codex.wordpress.org/Post_Formats
    add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

    // Enable HTML5 markup support
    // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    // Use main stylesheet for visual editor
    // To add custom styles edit /assets/styles/layouts/_tinymce.scss
    add_editor_style(Assets\asset_path('styles/main.css'));
}

add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

function new_submenu_class($menu)
{
    $menu = preg_replace('/ class="sub-menu"/', '/ class="dropdown-menu" /', $menu);
    return $menu;
}

add_filter('wp_nav_menu', __NAMESPACE__ . '\\new_submenu_class');


/**
 * Register sidebars
 */
function widgets_init()
{
    register_sidebar([
        'name' => __('Primary', 'sage'),
        'id' => 'sidebar-primary',
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ]);

    register_sidebar([
        'name' => __('Footer', 'sage'),
        'id' => 'sidebar-footer',
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ]);
}

add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar()
{
    static $display;

    isset($display) || $display = !in_array(true, [
        // The sidebar will NOT be displayed if ANY of the following return true.
        // @link https://codex.wordpress.org/Conditional_Tags
        is_404(),
        is_front_page(),
        is_page_template('template-custom.php'),
    ]);

    return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets()
{
    wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', false, null);
    wp_enqueue_style('select2-css', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css', false, null);
    wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    wp_enqueue_script('select2-js', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', ['jquery'], null, true);
    wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);
}

add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);

add_action('init', __NAMESPACE__ . '\\posty');
function posty()
{
    register_post_type('reference',
        array(
            'labels' => array(
                'name' => __('Reference'),
                'singular_name' => __('Reference')
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'reference'),
            'supports' => array(
                'title',
                'editor',
                'thumbnail'
            ),
        )
    );
    register_post_type('galerie',
        array(
            'labels' => array(
                'name' => __('Galerie'),
                'singular_name' => __('Galerie')
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'gallery'),
            'supports' => array(
                'title',
                'editor',
            ),
        )
    );
}


add_action("wpcf7_mail_components", __NAMESPACE__ . "\\wpcf7_do_something_else", 10, 3);

function wpcf7_do_something_else($components, $form, $object)
{

    if (strpos($form->title, 'Registracni') >= 0):

        $submission = \WPCF7_Submission::get_instance();
        $data = $submission->get_posted_data();

        $competitors = array();
        foreach ($data as $key => $field) {
            if (strpos($key, '-comp-')):
                $competitors[$key[strlen($key) - 1]][$key] = $field;
            endif;
        }
        $zavodnici = '';

        foreach ($competitors as $key => $competitor) {

            $zavodnici .= '<h2>' . __('Competitor', 'storypress') . ' ' . ($key + 1) . '</h2>';
            $zavodnici .= __('Name:', 'storypress') . ' ' . $competitor['name-comp-' . $key] . '<br>';
            $zavodnici .= __('Surname:', 'storypress') . ' ' . $competitor['surname-comp-' . $key] . '<br>';
            $zavodnici .= __('Birthdate:', 'storypress') . ' ' . $competitor['birthdate-comp-' . $key] . '<br>';
            $zavodnici .= __('Sex:', 'storypress') . ' ' . $competitor['sex-comp-' . $key] . '<br>';
            $zavodnici .= __('Weight (Kg):', 'storypress') . ' ' . $competitor['weight-comp-' . $key] . '<br>';
            $zavodnici .= __('Belt:', 'storypress') . ' ' . $competitor['belt-comp-' . $key] . '<br>';
            $zavodnici .= __('Year of training:', 'storypress') . ' ' . $competitor['training-comp-' . $key] . '<br>';
            $zavodnici .= __('Discipline:', 'storypress') . ' ';

            foreach ($competitor['discipline-comp-' . $key] as $discipline) {
                $zavodnici .= $discipline . ', ';
            }

            $zavodnici .= '<br>';
            $zavodnici .= __('Accomodation:', 'storypress') . ' ' . $competitor['accomodation-comp-' . $key] . '<br>';
            $zavodnici .= __('Price:', 'storypress') . ' ' . $competitor['price-comp-' . $key] . ' ' . $data['mena'] . '<br><br>';
        }
        $components['body'] = str_replace('[(ZAVODNICI)]', $zavodnici, $components['body']);
    endif;

    return $components;

}