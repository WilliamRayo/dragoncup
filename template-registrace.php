<?php
/**
 * Template Name: Registrace
 */
?>

<div class="container my-5 py-5">
  <?php while (have_posts()) : the_post(); 
    the_content();
   endwhile; ?>
  <div class="row js-competitor-clone d-none">
      <div class="col-12 mt-4">
        <h3><strong>1. Competitor</strong></h3>
      </div>
      <div class="form-group col-xl-4 col-lg-6 competitor-name">
        <label for="name-comp-x">Name</label>
        <input class="form-control" id="name-comp-x" type="text" name="name-comp-x">
      </div>
      <div class="form-group col-xl-4 col-lg-6 competitor-surname">
        <label for="surname-comp-x">Surname</label>
        <input class="form-control" id="surname-comp-x" name="surname-comp-x" type="text">
      </div>
      <div class="form-group col-xl-4 col-lg-6 competitor-birthdate">
        <label for="birthdate-comp-x">Birthdate</label>
        <input type="text" name="birthdate-comp-x" value="" class="form-control" id="birthdate-comp-x" placeholder="dd.mm.yyyy">
      </div>
      <div class="form-group col-xl-4 col-lg-6 competitor-sex">
        <label for="sex-comp-x">Sex</label>
        <select name="sex-comp-x" id="sex-comp-x" class="form-control">
            <option value="">---</option>
            <option value="male">Male</option>
            <option value="female">Female</option>
        </select>
      </div>
      <div class="form-group col-xl-4 col-lg-6 competitor-weight">
        <label for="weight-comp-x">Weight (Kg)</label>
        <input class="form-control" id="weight-comp-x" type="text" name="weight-comp-x">
      </div>
      <div class="form-group col-xl-4 col-lg-6 competitor-belt">
        <label for="belt-comp-x">Belt</label>
        <select class="form-control" name="belt-comp-x" id="belt-comp-x">
            <option value="">---</option>
            <option value="white">White</option>
            <option value="yellow">Yellow</option>
            <option value="orange">Orange</option>
            <option value="green">Green</option>
            <option value="blue">Blue</option>
            <option value="purple">Purple</option>
            <option value="red">Red</option>
            <option value="brown">Brown</option>
            <option value="black">Black belt</option>
        </select>
      </div>
      <div class="form-group col-xl-4 col-lg-6 competitor-training">
        <label for="training-comp-x">Year of training</label>
        <select class="form-control" name="training-comp-x" id="training-comp-x">
            <option value="">---</option>
            <option value="1-2_years">1-2 Years</option>
            <option value="3-5_years">3-5 Years</option>
            <option value="6-8_years">6-8 Years</option>
            <option value="9_years">9+ Years</option>
        </select>
      </div>
      <div class="form-group col-xl-4 col-lg-6 competitor-discipline">
        <label for="discipline-comp-x">Discipline</label>
        <select class="form-control js-multiple-select" name="discipline-comp-x" id="discipline-comp-x" multiple>
            <option value="DRAGON KICKBOX - SEMICONTACT">DRAGON KICKBOX - SEMICONTACT</option>
            <option value="DRAGON SANDA - LIGHTCONTACT">DRAGON SANDA - LIGHTCONTACT</option>
            <option value="Form Hard">Form Hard</option>
            <option value="Form Soft">Form Soft</option>
            <option value="Form Synchron">Form Synchron</option>
            <option value="Form Weapon">Form Weapon</option>
            <option value="Horse Stance">Horse Stance</option>
            <option value="Self Defence">Self Defence</option>
            <option value="KIHON">KIHON</option>
        </select>
      </div>
      <div class="form-group col-xl-4 col-lg-6 competitor-accommodation">
        <label for="accomodation-comp-x">Accomodation</label>
        <select class="form-control" name="accomodation-comp-x" id="accomodation-comp-x">
            <option value="">---</option>
            <option value="yes">Yes</option>
            <option value="no">No</option>
        </select>
      </div>
      <div class="col-xl-4 col-lg-6 ml-xl-auto pt-3">
          <div class="alert bg-info text-right competitor-price">Price: <span><strong>0</strong></span> Euro</div>
          <input type="text" name="price-comp-x" class="input-price-comp d-none">
      </div>
    </div>
  </div>
</div>